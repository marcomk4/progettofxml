/*
 *   V2d.java
 *
 * Copyright 2000-2001-2002  aliCE team at deis.unibo.it
 *
 * This software is the proprietary information of deis.unibo.it
 * Use is subject to license terms.
 *
 */
package paranoid.common;

/**
 *
 * 2-dimensional vector
 * objects are completely state-less
 *
 */
public class Vel2d {

    private double x,y;
    
    public Vel2d(double x,double y){
        this.x=x;
        this.y=y;
    }

    public Vel2d(Pos2d to, Pos2d from){
        this.x=to.getX()-from.getX();
        this.y=to.getY()-from.getY();
    }

    public Vel2d sum(Vel2d v){
        return new Vel2d(x+v.x,y+v.y);
    }

    public double module(){
        return (double)Math.sqrt(x*x+y*y);
    }

    public Vel2d getNormalized(){
        double module=(double)Math.sqrt(x*x+y*y);
        return new Vel2d(x/module,y/module);
    }

    public Vel2d mul(double fact){
        return new Vel2d(x*fact,y*fact);
    }

    public double getX() {
        return this.x;
    }
    
    public double getY() {
        return this.y;
    }
    
    public String toString(){
        return "V2d("+x+","+y+")";
    }
}
