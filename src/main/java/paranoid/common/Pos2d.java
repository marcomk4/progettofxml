package paranoid.common;

/**
 *
 * 2-dimensional point
 * objects are completely state-less
 *
 */
public class Pos2d {

    private double x,y;

    public Pos2d(double x,double y){
        this.x=x;
        this.y=y;
    }

    public Pos2d sum(Vel2d v){
        return new Pos2d(x+v.getX(),y+v.getY());
    }

    public Vel2d sub(Pos2d v){
        return new Vel2d(x-v.x,y-v.y);
    }

    public String toString(){
        return "Pos2d("+x+","+y+")";
    }
    
    public double getX() {
        return this.x;
    }
    
    public double getY() {
        return this.y;
    }

}
