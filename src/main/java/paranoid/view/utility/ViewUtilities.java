package paranoid.view.utility;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.SplitPane;
import paranoid.controller.GUIController;

public enum ViewUtilities {

    MENU("layouts/menu.fxml"),
    GAME("layouts/game.fxml");

    private GUIController guiController;
    private SplitPane layout;

    private ViewUtilities(final String name) {
        try {
            FXMLLoader loader = new FXMLLoader(ClassLoader.getSystemResource(name));
            this.layout = loader.load();
            this.guiController = loader.getController();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public SplitPane getLayout() {
        return this.layout;
    }
    
    public GUIController getController() {
        return this.guiController;
    }
}
