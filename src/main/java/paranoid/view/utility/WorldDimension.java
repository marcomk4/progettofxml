package paranoid.view.utility;

import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;

public final class WorldDimension {

    private WorldDimension() {

    }

    public static final double SCREEN_WIDTH;
    public static final double SCREEN_HEIGHT;
    public static final double CANVAS_WIDTH;
    public static final double CANVAS_HEIGHT;
    public static final double WORLD_WIDTH;
    public static final double WORLD_HEIGHT;


    static {
        Rectangle2D screenRes = Screen.getPrimary().getBounds();
        SCREEN_WIDTH = (screenRes.getWidth() / 1.5);
        SCREEN_HEIGHT = (screenRes.getHeight() / 1.1);
        WORLD_WIDTH = 600;
        WORLD_HEIGHT = 600;
        CANVAS_WIDTH = SCREEN_WIDTH / 1.5;
        CANVAS_HEIGHT = SCREEN_WIDTH / 1.5;
    }

}
