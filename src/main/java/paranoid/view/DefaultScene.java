package paranoid.view;

import javafx.scene.Scene;
import paranoid.view.utility.ViewUtilities;
import paranoid.view.utility.WorldDimension;

public class DefaultScene extends Scene {

    public DefaultScene() {
        super(ViewUtilities.MENU.getLayout(), WorldDimension.SCREEN_WIDTH, WorldDimension.SCREEN_HEIGHT);
    }
}
