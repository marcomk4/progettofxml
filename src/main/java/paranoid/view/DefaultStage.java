package paranoid.view;

import javafx.scene.Scene;
import javafx.stage.Stage;
import paranoid.view.utility.ViewUtilities;
import paranoid.view.utility.WorldDimension;

public class DefaultStage extends Stage{

    public DefaultStage() {
        super();
        this.setTitle("Paranoid");
        this.setScene(new MainScene());
        this.sizeToScene();
        this.show();
    }
    
    public class MainScene extends Scene {

        public MainScene() {
            super(ViewUtilities.MENU.getLayout(), WorldDimension.SCREEN_WIDTH, WorldDimension.SCREEN_HEIGHT);
        }
    }

}
