package paranoid.model.gameobj;

import paranoid.common.Pos2d;
import paranoid.common.Vel2d;
import paranoid.model.World;
import paranoid.model.input.InputComponent;
import paranoid.model.input.InputController;
import paranoid.model.physics.PhysicsComponent;

public abstract class GameObj {

    private Pos2d pos;
    private Vel2d vel;
    private int height;
    private int width;
    private PhysicsComponent phys;
    private InputComponent input;
    
    public GameObj(Pos2d pos, Vel2d vel, int width, int height, PhysicsComponent phys, InputComponent input){
            this.pos = pos;
            this.vel = vel;
            this.width = width;
            this.height = height;
            this.phys = phys;
            this.input = input;
    }
    
    public void setPos(Pos2d pos){
            this.pos = pos;
    }

    public void setVel(Vel2d vel){
            this.vel = vel;
    }
    
    public Pos2d getCurrentPos(){
            return pos;
    }
    
    public Vel2d getCurrentVel(){
            return vel;
    }

    public int getHeight() {
        return this.height;
    }
    
    public double getWidth() {
        return this.width;
    }
    
    public PhysicsComponent getPhysicsComponent() {
        return this.phys;
    }
    
    public InputComponent getInputComponent() {
        return this.input;
    }
    
    abstract public void updatePhysics(int dt, World world);
    
    abstract public void updateInput(GameObj player, InputController controller);
}
