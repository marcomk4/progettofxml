package paranoid.model.gameobj;

import paranoid.common.Pos2d;
import paranoid.common.Vel2d;
import paranoid.model.World;
import paranoid.model.input.DummyInputComponent;
import paranoid.model.input.InputController;
import paranoid.model.physics.BallPhysicsComponent;

public class Ball extends GameObj{

    public Ball(Pos2d pos, Vel2d vel, int width, int height) {
        super(pos, vel, width, height, new BallPhysicsComponent(), new DummyInputComponent());
    }
    
    public void flipVelOnY(){
        this.setVel(new Vel2d(this.getCurrentVel().getX(), -this.getCurrentVel().getY()));
    }

    public void flipVelOnX(){
        this.setVel(new Vel2d(-this.getCurrentVel().getX(), this.getCurrentVel().getY()));
    }

    @Override
    public void updatePhysics(int dt, World world) {
        this.getPhysicsComponent().update(dt, this, world);
    }

	@Override
	public void updateInput(GameObj player, InputController controller) {
		this.getInputComponent().update(this, controller);
	}

}
