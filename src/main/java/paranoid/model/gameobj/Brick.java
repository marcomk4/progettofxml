package paranoid.model.gameobj;

import java.util.Optional;

import paranoid.common.Collision;
import paranoid.common.Pos2d;
import paranoid.common.Vel2d;
import paranoid.model.World;
import paranoid.model.input.DummyInputComponent;
import paranoid.model.input.InputController;
import paranoid.model.physics.DummyPhysicsComponent;
import paranoid.model.physics.PhysicsComponent;

public class Brick extends GameObj{

	private Optional<Collision> lastZonePresence;
	private int pointsEarned;
	
	public Brick(Pos2d pos, int width, int height, int pointsEarned, PhysicsComponent phys) {
		super(pos, new Vel2d(0, 0), width, height, new DummyPhysicsComponent(), new DummyInputComponent());
		this.lastZonePresence = Optional.empty();
		this.pointsEarned = pointsEarned;
	}

	@Override
	public void updatePhysics(int dt, World world) {
		this.getPhysicsComponent().update(dt, this, world);
	}
	
	@Override
	public void updateInput(GameObj player, InputController controller) {
		this.getInputComponent().update(this, controller);
	}
	
	public void setLastZonePresence(Collision collision) {
		this.lastZonePresence = Optional.of(collision);
	}
	
	public Optional<Collision> getLastZonePresence() {
		return this.lastZonePresence;
	}
	
	public int getPointsEarned() {
		return this.pointsEarned;
	}

}
