package paranoid.model.gameobj;

import paranoid.common.Direction;
import paranoid.common.Pos2d;
import paranoid.common.Vel2d;
import paranoid.model.World;
import paranoid.model.input.InputController;
import paranoid.model.input.PlayerInputComponent;
import paranoid.model.physics.PlayerPhysicsComponent;

public class Player extends GameObj{

	private Direction currentDirection;
	
	public Player(Pos2d pos, int width, int height) {
		super(pos, new Vel2d(0, 0), width, height, new PlayerPhysicsComponent(), new PlayerInputComponent());
		this.currentDirection = Direction.STOP;
	}

	@Override
    public void updatePhysics(int dt, World world) {
        this.getPhysicsComponent().update(dt, this, world);
    }

	@Override
	public void updateInput(GameObj player, InputController controller) {
		if(controller.isMoveLeft()) {
			this.currentDirection = Direction.LEFT;
		} else if(controller.isMoveRight()) {
			this.currentDirection = Direction.RIGHT;
		} else {
			this.currentDirection = Direction.STOP;
		}

		this.getInputComponent().update(this, controller);
	}

	public Direction getCurrentDirection(){
		return this.currentDirection;
	}
	
}
