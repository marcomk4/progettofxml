package paranoid.model;

import paranoid.common.Pos2d;

public class WorldBox {

    private Pos2d upperLefCorner;
    private Pos2d bottomRightCorner;
    
    public WorldBox(Pos2d upperLeftCorner, Pos2d bottomRightCorner) {
        this.bottomRightCorner = bottomRightCorner;
        this.upperLefCorner = upperLeftCorner;
    }
    
    public Pos2d getBottomRightCorner() {
        return this.bottomRightCorner;
    }
    
    public Pos2d getUpperleftCorner() {
        return this.upperLefCorner;
    }
    
    public double getWidth() {
        return (int) (bottomRightCorner.getY() - upperLefCorner.getY());
    }
    
    public int getHeight() {
        return (int) (bottomRightCorner.getX() - upperLefCorner.getX());
    }
    

    
}
