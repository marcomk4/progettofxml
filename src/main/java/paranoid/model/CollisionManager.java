package paranoid.model;

import java.util.Optional;

import paranoid.common.Collision;
import paranoid.common.Direction;
import paranoid.common.Pair;
import paranoid.model.gameobj.Brick;
import paranoid.model.gameobj.GameObj;
import paranoid.model.gameobj.Player;

public class CollisionManager {

public CollisionManager() {}
    
    
    public Optional<Collision> checkCollisionWithBoundaries(WorldBox border, GameObj entity){
        if (entity.getCurrentPos().getY() < border.getUpperleftCorner().getY()){
            return Optional.of(Collision.ORIZONTAL);
        } else if (entity.getCurrentPos().getY() + entity.getHeight() > border.getBottomRightCorner().getY()) {
                return Optional.of(Collision.ORIZONTAL);
            } else if (entity.getCurrentPos().getX() < border.getUpperleftCorner().getX()){
            return Optional.of(Collision.VERTICAL);
        } else if (entity.getCurrentPos().getX() + entity.getWidth() > border.getBottomRightCorner().getX()){
            return Optional.of(Collision.VERTICAL);
        } else {
            return Optional.empty();
        }
    }
    
    public Optional<Pair<Brick, Collision>> checkCollisionWithTiles(Brick tile, GameObj entity){
        boolean checkLeft = false;
        boolean checkRight = false;
        boolean checkBot = false;
        boolean checkTop = false;
        if (entity.getCurrentPos().getX() <= tile.getCurrentPos().getX() + tile.getWidth()) {
            checkLeft = true;
        } else {
            tile.setLastZonePresence(Collision.VERTICAL);
        }
        if (entity.getCurrentPos().getX() + entity.getWidth() >= tile.getCurrentPos().getX()) {
            checkRight = true;
        } else {
            tile.setLastZonePresence(Collision.VERTICAL);
        }
        if (entity.getCurrentPos().getY() + entity.getHeight() >= tile.getCurrentPos().getY()) {
            checkBot = true;
        } else {
            tile.setLastZonePresence(Collision.ORIZONTAL);
        }
        if (entity.getCurrentPos().getY() <= tile.getCurrentPos().getY() + tile.getHeight()) {
            checkTop = true;
        } else {
            tile.setLastZonePresence(Collision.ORIZONTAL);
        }
        
        if (checkLeft && checkRight && checkBot && checkTop) {
            return Optional.of(new Pair<>(tile, tile.getLastZonePresence().get()));
        }
        return Optional.empty();
    }
    
    public Optional<Direction> checkCollisionWithPlayers(Player player, GameObj entity) {
        if(player.getCurrentPos().getX() < entity.getCurrentPos().getX() + entity.getWidth() &&
            player.getCurrentPos().getX() + player.getWidth() > entity.getCurrentPos().getX() &&
            player.getCurrentPos().getY() < entity.getCurrentPos().getY() + entity.getHeight() &&
            player.getCurrentPos().getY() + player.getHeight() > entity.getCurrentPos().getY()){              
            int CenterOfGravity = (int) (entity.getCurrentPos().getX() + (entity.getWidth()/2));
            int zone = (int) (player.getWidth()/4);
            if(CenterOfGravity < player.getCurrentPos().getX() + (zone*1) && CenterOfGravity > player.getCurrentPos().getX()){
                return Optional.of(Direction.LEFT_EDGE);
            } else if(CenterOfGravity < player.getCurrentPos().getX() + (zone*2) && CenterOfGravity > player.getCurrentPos().getX() + (zone*1)) {
                return Optional.of(Direction.LEFT);
            } else if(CenterOfGravity < player.getCurrentPos().getX() + (zone*3) && CenterOfGravity > player.getCurrentPos().getX() + (zone*2)) {
                return Optional.of(Direction.RIGHT);
            } else if(CenterOfGravity < player.getCurrentPos().getX() + (zone*4) && CenterOfGravity > player.getCurrentPos().getX() + (zone*3)) {
                return Optional.of(Direction.RIGHT_EDGE);
            }   
        }
        return Optional.empty();
    }
}
