package paranoid.model.physics;

import java.util.Optional;

import paranoid.common.Collision;
import paranoid.common.Pos2d;
import paranoid.common.Vel2d;
import paranoid.model.World;
import paranoid.model.events.HitBorderEvent;
import paranoid.model.gameobj.GameObj;
import paranoid.model.gameobj.Player;

public class PlayerPhysicsComponent implements PhysicsComponent{

    private static double SCALER = 0.001;
    
    @Override
    public void update(int dt, GameObj gameObj, World world) {

        Player player = (Player) gameObj;
        Pos2d pos = player.getCurrentPos();
        Vel2d vel = player.getCurrentVel();
        Pos2d oldPos = player.getCurrentPos();

        player.setPos(pos.sum(vel.mul(SCALER*dt)));
        Optional<Collision> borderCollisionInfo = world.checkCollisionWithBoundaries(gameObj);
        
    }

}
