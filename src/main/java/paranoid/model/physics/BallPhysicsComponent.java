package paranoid.model.physics;

import java.util.Optional;

import paranoid.common.Collision;
import paranoid.common.Direction;
import paranoid.common.Pos2d;
import paranoid.common.Pair;
import paranoid.common.Vel2d;
import paranoid.model.events.HitBorderEvent;
import paranoid.model.events.HitBrickEvent;
import paranoid.model.gameobj.Ball;
import paranoid.model.gameobj.GameObj;
import paranoid.model.gameobj.Brick;
import paranoid.model.World;

public class BallPhysicsComponent implements PhysicsComponent{

private static double SCALER = 0.001;
    
    @Override
    public void update(int dt, GameObj boundingObject, World w) {
        Pos2d oldPos = boundingObject.getCurrentPos();
        
        Ball ball = (Ball) boundingObject;
        Pos2d pos = ball.getCurrentPos();
        Vel2d vel = ball.getCurrentVel();
        ball.setPos(pos.sum(vel.mul(SCALER*dt)));

        Optional<Collision> borderCollisionInfo = w.checkCollisionWithBoundaries(boundingObject);
        if(borderCollisionInfo.isPresent()) {
            ball.setPos(oldPos);
            if(borderCollisionInfo.get().equals(Collision.ORIZONTAL)) {
                ball.flipVelOnY();
            } else {
                ball.flipVelOnX();
            }
            w.notifyEvent(new HitBorderEvent());
        }
        
        Optional<Pair<Brick, Collision>> tileCollisionInfo = w.checkCollisionWithTiles(boundingObject);
        if(tileCollisionInfo.isPresent()) {
            ball.setPos(oldPos);
            Pair<Brick, Collision> content = tileCollisionInfo.get();
            if(content.getY().equals(Collision.ORIZONTAL)) {
                ball.flipVelOnY();
            } else {
                ball.flipVelOnX();
            }
            w.notifyEvent(new HitBrickEvent(content.getX()));
        }
        
        Optional<Direction> playerInfo = w.checkCollisionWithPlayers(boundingObject);
        if(playerInfo.isPresent()) {
            ball.setPos(oldPos);            
            ball.flipVelOnY();
            if(playerInfo.get().equals(Direction.LEFT_EDGE)) {
                ball.setVel(new Vel2d(-200, -100));
            } else if(playerInfo.get().equals(Direction.LEFT)) {
                ball.setVel(new Vel2d(-100, -200));
            } else if(playerInfo.get().equals(Direction.RIGHT)) {
                ball.setVel(new Vel2d(100, -200));
            } else if(playerInfo.get().equals(Direction.RIGHT_EDGE)) {
                ball.setVel(new Vel2d(200, -100));
            }
        }
    }

}
