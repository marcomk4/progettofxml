package paranoid.model.physics;

import paranoid.model.World;
import paranoid.model.gameobj.GameObj;

public class DummyPhysicsComponent implements PhysicsComponent{

	@Override
	public void update(int dt, GameObj gameObj, World w) {
		//un componente fisico che non fa nulla
	}

}
