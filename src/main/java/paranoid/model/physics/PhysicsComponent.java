package paranoid.model.physics;

import paranoid.model.World;
import paranoid.model.gameobj.GameObj;

public interface PhysicsComponent {
    
    public void update(int dt, GameObj gameObj, World world);

}
