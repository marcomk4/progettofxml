package paranoid.model;

import java.util.ArrayList;
import java.util.List;

import paranoid.common.Pos2d;
import paranoid.common.Vel2d;
import paranoid.model.gameobj.Ball;
import paranoid.model.gameobj.Brick;
import paranoid.model.gameobj.Player;
import paranoid.model.levels.LevelBuilder;
import paranoid.view.utility.WorldDimension;

public class GameState {
	
    private static final int PROGRESS = 100;

    private int score;
    private int multiplier;
    private int lives;
    
    private World world;
   
    private GameFactory gb;
    
    public GameState(){
        
    }
    
    public void gameInit() {
        this.score = 0;
        this.multiplier = 1;
        this.lives = 1;
        this.gb = GameFactory.getInstance();
        LevelBuilder levelBuilder = new LevelBuilder(WorldDimension.WORLD_WIDTH, WorldDimension.WORLD_HEIGHT);
        
        List<Player> playerContainer = new ArrayList<>();
        playerContainer.add(new Player(new Pos2d(300, 540), 60, 10));
        
        List<Ball> ballContainer = new ArrayList<>();
        ballContainer.add(new Ball(new Pos2d(330, 500), new Vel2d(100, -200), 10, 10));
        
        List<Brick> tileContainer = new ArrayList<>();
        
        for (int y = 0; y <= 8; y++) {
            for (int x = 0; x <= y; x++) {
                tileContainer.add(gb.createBrick(new Pos2d(61 * x, 31 * y)));
            }
        }
        
        levelBuilder.addTiles(tileContainer)
                    .addBalls(ballContainer)
                    .addPlayers(playerContainer);
        
        world = new World(this, levelBuilder.buildLevel());
    }
    
    public World getWorld(){
        return world;
    }
    
    public void incMultiplier(){
        this.multiplier++;
    }

    public void flatMultiplier(){
        this.multiplier = 1;
    }
    
    public void incScore(){
        this.score += PROGRESS * multiplier;
    }

    public void decScore(){
        this.score -= PROGRESS;
    }
    
    public int getScore(){
        return this.score;
    }
    
    public int getMultiplier(){
        return this.multiplier;
    }
    
    public int getLives(){
        return this.lives;
    }
    
    public void update(int dt){
            world.updateState(dt);
    }

}
