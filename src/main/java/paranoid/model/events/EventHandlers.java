package paranoid.model.events;

import java.util.ArrayList;
import java.util.List;

import paranoid.model.GameState;
import paranoid.model.events.Event;
import paranoid.model.events.HitBorderEvent;
import paranoid.model.events.HitBrickEvent;

public class EventHandlers {

    private GameState gameState;
    private List<Event> events = new ArrayList<>();
    
    public EventHandlers(final GameState gameState) {
        this.gameState = gameState;
    }
    
    public void resolveEvent() {
        events.stream().forEach(ev -> {
            if (ev instanceof HitBorderEvent){
                gameState.flatMultiplier();
            } else if (ev instanceof HitBrickEvent){
                gameState.incScore();
                gameState.incMultiplier();
                HitBrickEvent hit = (HitBrickEvent)ev;
                gameState.getWorld().removeTile(hit.getCollisionObject());
            }
        });
        events.clear();
    }
    
    public void addEvent(Event event) {
        this.events.add(event);
    }
    
    
    
}
