package paranoid.model.events;

public interface WorldEventListener {

    void notifyEvent(Event ev);

}
