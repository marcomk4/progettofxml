package paranoid.model.events;

import paranoid.model.gameobj.Brick;

public class HitBrickEvent implements Event{

    private Brick collisionObject;
    
    public HitBrickEvent(final Brick collisionObject) {
        this.collisionObject = collisionObject;
    }
    
    public Brick getCollisionObject() {
        return this.collisionObject;
    }
    
}
