package paranoid.model;

import paranoid.common.Pos2d;
import paranoid.model.gameobj.Brick;
import paranoid.model.physics.DummyPhysicsComponent;

public final class GameFactory {

    private static GameFactory instance;
    
    private GameFactory() {
        
    }
    public static GameFactory getInstance(){
        if (instance == null){
            instance = new GameFactory();
        }
        return instance;
    }
    
    public Brick createBrick(Pos2d pos) {
        return new Brick(pos, 60, 30, 10, new DummyPhysicsComponent());
    }
}
