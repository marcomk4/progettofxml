package paranoid.model.levels;

import java.util.ArrayList;
import java.util.List;

import paranoid.common.Pos2d;
import paranoid.model.levels.Level;
import paranoid.model.levels.LevelBuilder;
import paranoid.model.gameobj.Ball;
import paranoid.model.WorldBox;
import paranoid.model.gameobj.Player;
import paranoid.model.gameobj.Brick;

public class LevelBuilder {

    private List<Ball> balls = new ArrayList<>();    
    private List<Brick> bricks = new ArrayList<>();
    private List<Player> players = new ArrayList<>();
    private WorldBox border;
    
    public LevelBuilder(final double width, final double height) {
        this.border = new WorldBox(new Pos2d(0, 0), new Pos2d(width, height));
    }
    
    public Level buildLevel() {
        return new Level(border, bricks, balls, players); 
    }
    
    public LevelBuilder addBalls(List<Ball> balls) {
        this.balls.addAll(balls);
        return this;
    }
    
    public LevelBuilder addBall(Ball ball) {
        this.balls.add(ball);
        return this;
    }

    public LevelBuilder addTiles(List<Brick> tiles) {
        this.bricks.addAll(tiles);
        return this;
    }
    
    public LevelBuilder addTiles(Brick tile) {
        this.bricks.add(tile);
        return this;
    }
    
    public LevelBuilder addPlayers(List<Player> players) {
        this.players.addAll(players);
        return this;
    }
    
    public LevelBuilder addPlayer(Player player) {
        this.players.add(player);
        return this;
    }
    
    public LevelBuilder removePlayer(Player player) {
        this.players.remove(player);
        return this;
    }
}
