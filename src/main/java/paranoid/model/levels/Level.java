package paranoid.model.levels;

import java.util.ArrayList;
import java.util.List;

import paranoid.model.gameobj.Ball;
import paranoid.model.WorldBox;
import paranoid.model.gameobj.Player;
import paranoid.model.gameobj.Brick;;

public class Level {

    private List<Ball> balls = new ArrayList<>(); 
    private List<Brick> bricks = new ArrayList<>();
    private List<Player> players = new ArrayList<>();
    private WorldBox border;
    
    public Level(WorldBox border, List<Brick> bricks, List<Ball> balls, List<Player> players) {
        this.border = border;
        this.bricks.addAll(bricks);
        this.balls.addAll(balls);
        this.players.addAll(players);
    }

    public List<Ball> getBalls() {
        return balls;
    }

    public List<Brick> getTiles() {
        return bricks;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public WorldBox getBorder() {
        return border;
    }
}
