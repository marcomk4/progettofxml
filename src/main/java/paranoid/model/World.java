package paranoid.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import paranoid.common.Collision;
import paranoid.common.Direction;
import paranoid.common.Pair;
import paranoid.model.events.Event;
import paranoid.model.events.EventHandlers;
import paranoid.model.events.WorldEventListener;
import paranoid.model.gameobj.Ball;
import paranoid.model.gameobj.Brick;
import paranoid.model.gameobj.GameObj;
import paranoid.model.gameobj.Player;
import paranoid.model.input.InputController;
import paranoid.model.levels.Level;

public class World implements WorldEventListener{

    private List<Ball> balls = new ArrayList<>();    
    private List<Brick> tiles = new ArrayList<>();
    private List<Player> players = new ArrayList<>();    
    private WorldBox border;
    private CollisionManager collisionManager;
    private EventHandlers eventHandlers;

    public World(final GameState gameState, final Level level) {
        this.balls = level.getBalls();
        this.tiles = level.getTiles();
        this.players = level.getPlayers();
        this.border = level.getBorder();
        this.collisionManager = new CollisionManager();
        this.eventHandlers = new EventHandlers(gameState);
    }
    
    public Optional<Collision> checkCollisionWithBoundaries(GameObj entity){
        return this.collisionManager.checkCollisionWithBoundaries(border, entity);
    }
    
    public Optional<Pair<Brick, Collision>> checkCollisionWithTiles(GameObj entity){
        Optional<Pair<Brick, Collision>> collisionResult = Optional.empty();
        for(Brick tile : this.tiles) {
            collisionResult = this.collisionManager.checkCollisionWithTiles(tile, entity);
            if(collisionResult.isPresent()) {
                return collisionResult;
            }
        }
        return collisionResult; 
    }
    
    public Optional<Direction> checkCollisionWithPlayers(GameObj entity) {
        Optional<Direction> collisionResult = Optional.empty();
        for(Player player : this.players) {
            collisionResult = this.collisionManager.checkCollisionWithPlayers(player, entity);
            if(collisionResult.isPresent()) {
                return collisionResult;
            }
        }
        return Optional.empty();
    }
        
    public void updateState(int dt){
        this.getSceneEntities().forEach(i -> i.updatePhysics(dt, this));
    }
    
    public void movePayer(InputController controller) {
        this.getSceneEntities().forEach(i -> i.updateInput(i, controller));
    }

    public WorldBox getBorder() {
        return this.border;
    }
    
    public void removeTile(Brick tile) {
        this.tiles.remove(tile);
    }
    
    public List<GameObj> getSceneEntities(){
        List<GameObj> entities = new ArrayList<GameObj>();
        entities.addAll(balls);
        entities.addAll(players);
        entities.addAll(tiles);
        return entities;
    }

    @Override
    public void notifyEvent(Event ev) {
        this.eventHandlers.addEvent(ev);
        
    }
    
    public EventHandlers getEvHandler() {
        return this.eventHandlers;
    }
    
}
