package paranoid.model.input;

import paranoid.common.Vel2d;
import paranoid.model.gameobj.GameObj;

public class PlayerInputComponent implements InputComponent {
	
	private static int PLAYER_AGILITY = 300;  
	
	public void update(GameObj player, InputController ctrl){
		if (ctrl.isMoveLeft()){
		    player.setVel(new Vel2d(-1,0).mul(PLAYER_AGILITY));
		} else if (ctrl.isMoveRight()){
            player.setVel(new Vel2d(1,0).mul(PLAYER_AGILITY));
		} else {
            player.setVel(new Vel2d(0,0));
		}
	}
	
}
