package paranoid.model.input;

import paranoid.model.World;
import paranoid.model.gameobj.GameObj;

public class DummyInputComponent implements InputComponent{

	@Override
	public void update(GameObj ball, InputController c) {
		//un componente di input che non fa nulla
	}

}
