package paranoid.model.input;

public interface InputController {

	boolean isMoveLeft();
	
	boolean isMoveRight();
	
}
