package paranoid.model.input;

import paranoid.model.World;
import paranoid.model.gameobj.GameObj;

public interface InputComponent {
	
	void update(GameObj ball, InputController c);

}
