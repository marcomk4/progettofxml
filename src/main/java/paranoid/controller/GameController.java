package paranoid.controller;

import java.util.List;

import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import paranoid.common.Pos2d;
import paranoid.view.utility.WorldDimension;
import paranoid.model.gameobj.Ball;
import paranoid.model.gameobj.Brick;
import paranoid.model.gameobj.GameObj;
import paranoid.model.gameobj.Player;

public final class GameController implements GUIController {
    
    private static final double RATIO = WorldDimension.CANVAS_WIDTH / WorldDimension.WORLD_WIDTH;
    private GraphicsContext gc;

    @FXML
    private Canvas canvas;
    public GameController() {

    }

    @FXML
    public void initialize() {
        this.canvas.setWidth(WorldDimension.CANVAS_WIDTH);
        this.canvas.setHeight(WorldDimension.CANVAS_HEIGHT);
        this.gc = canvas.getGraphicsContext2D();
        
    }

    public void render(List<GameObj> gameEntities) {
        drawWorld(gameEntities);
    }

    private void drawWorld(List<GameObj> gameEntities) {

        gc.clearRect(0, 0, WorldDimension.CANVAS_WIDTH, WorldDimension.CANVAS_HEIGHT);
        gc.setFill(Color.AQUAMARINE);
        gameEntities.stream().forEach(e -> {
            Pos2d position = e.getCurrentPos();
            double xPos = getXinPixel(position);
            double yPos = getYinPixel(position);
            double w = getDeltaXinPixel(e.getWidth());
            double h = getDeltaYinPixel(e.getHeight());
            if (e instanceof Player) {
                gc.fillRect(xPos, yPos, w, h);
            } else if (e instanceof Ball) {
                gc.fillOval(xPos, yPos, w, h);
            } else if (e instanceof Brick) {
                gc.fillRect(xPos, yPos, w, h);
            }

        });
    }

    private double getXinPixel(final Pos2d pos) {
        return (pos.getX() * RATIO);
    }
    
    private double getYinPixel(final Pos2d pos) {
        return (pos.getY() * RATIO);
    }
    
    private double getDeltaXinPixel(final double dx) {
        return (dx * RATIO);
    }
    
    private double getDeltaYinPixel(final double dy) {
        return (dy * RATIO);
    }

}
