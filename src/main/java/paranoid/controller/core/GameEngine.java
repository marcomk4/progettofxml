package paranoid.controller.core;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import paranoid.controller.GameController;
import paranoid.model.GameState;
import paranoid.model.input.KeyboardInputController;
import paranoid.view.utility.ViewUtilities;

public class GameEngine  implements Runnable{

    private long period = 20;   
    private GameState gameState;
    private Scene scene;
    private GameController gameController;
    private KeyboardInputController controller;
    
    public GameEngine(final Scene scene) {
    	//this.events = new ArrayList<Event>();
        this.gameState = new GameState();
        this.controller = new KeyboardInputController();
        this.scene = scene;
        this.scene.setRoot(ViewUtilities.GAME.getLayout());
        this.gameController = (GameController)ViewUtilities.GAME.getController();
        notifyInputEvent();
    }

    @Override
    public void run() {
        this.gameState.gameInit();
        long prevTime = System.currentTimeMillis();
        while (true) {
                long currentTime = System.currentTimeMillis();
                int timeElapsed = (int) (currentTime - prevTime);
                processInput();
                updateGame(timeElapsed);
                render();
                waitForNextFrame(currentTime);
                prevTime = currentTime;
        }
    }

    private void waitForNextFrame(long currentTime){
            long dt = System.currentTimeMillis() - currentTime;
            if (dt < period){
                    try {
                            Thread.sleep(period-dt);
                    } catch (Exception ex){}
            }
    }
    
    private void processInput(){
    	gameState.getWorld().movePayer(controller);
    }
    
    private void updateGame(int timeElapsed){
        gameState.getWorld().updateState(timeElapsed);
        this.gameState.getWorld().getEvHandler().resolveEvent();
        //checkEvents();
    }

    private void render(){
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                gameController.render(gameState.getWorld().getSceneEntities());

            }

        });
    }

    private void notifyInputEvent() {
        this.scene.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.SPACE) {
                    
                }else if (event.getCode() == KeyCode.RIGHT) {
                    controller.notifyMoveRight();
                }else if (event.getCode() == KeyCode.LEFT) {
                    controller.notifyMoveLeft();
                }
            }

        });

        this.scene.setOnKeyReleased(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.SPACE) {
                    
                }else if (event.getCode() == KeyCode.RIGHT) {
                    controller.notifyNoMoreMoveRight();
                }else if (event.getCode() == KeyCode.LEFT) {
                    controller.notifyNoMoreMoveLeft();
                }
            }

        });
    }
}
