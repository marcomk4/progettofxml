package paranoid.main;

/**
 * This class represents the Launcher of the system, to bypass JAVA 11 modules constraints.
 */
public final class Launcher {

    private Launcher() { }

    /**
     * @param args 
     * parametro non usato
     */
    public static void main(final String[] args) {
        Paranoid.main(args);
    }
}
