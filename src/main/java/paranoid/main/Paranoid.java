package paranoid.main;

import javafx.application.Application;
import javafx.stage.Stage;
import paranoid.view.DefaultStage;


/**
 * ARKANOID GAME.
 * 
 *
 */
public class Paranoid extends Application {

    /**
     * {@inheritDoc}
     */
    @Override
    public void start(final Stage stage) throws Exception {
        new DefaultStage();
    }
/**
 * Paranoid Menu.
 * @param args
 *      nothing
 */
    public static void main(final String[] args) {
        launch();
    }
}
